import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int width, int height, int locationA, int locationB){
        super.setSize(width, height);
        super.setLocation(locationA, locationB);
    }

    //Implementation

    /**
     * This function returns the area of the rectangle
     * @return width*height (area)
     */
    public int getArea(){
        return width * height;
    }

    /**
     * This functions calculates returns the perimeter length.
     * @return the perimeter of the rectangle
     */
    public int getPerimeter(){
        int perimiter = width*2 + height*2;
        return perimiter;
    }

}
