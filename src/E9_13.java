import java.awt.Rectangle;
import java.util.Scanner;

public class E9_13 {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        int iHeight;
        int iWidth ;

        for(int i = 0; i < 3; i++){
            System.out.printf("Enter the height and width of the rectangle separated by a space: ");
            iHeight = keyboard.nextInt();
            iWidth = keyboard.nextInt();
            BetterRectangle example = new BetterRectangle(iWidth,iHeight,0, 0);
            if((iHeight < 0 || iWidth < 0)){
                System.out.printf("Input value invalid: Negative numbers unallowed\n");
                continue;
            }

            System.out.printf("The expected value for area is: %d\n", expectedArea(iHeight, iWidth));
            System.out.printf("Your calculated value of the area is; %d\n", example.getArea());
            System.out.printf("The expected value for perimeter is: %d\n", expectedPerimeter(iHeight, iWidth));
            System.out.printf("Your calculated value of the area is; %d\n", example.getPerimeter());
        }

    }

    /**
     * Function calculates the area of a rectangle
     * @param height
     * @param width
     * @return area
     */
    public static int expectedArea(int height, int width){
        return height * width;
    }

    /**
     * Function calculates perimeter of a rectangle
     * @param height
     * @param width
     * @return perimeter
     */
    public static int expectedPerimeter(int height, int width){
        return height * 2 + width * 2;
    }
}

/**
 * Sample Output:
 *
 * "C:\Program Files\Java\jdk-15.0.1\bin\java.exe" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2021.2.1\lib\idea_rt.jar=13766:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2021.2.1\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\16692\IdeaProjects\E9_13\out\production\E9_13 E9_13
 * Enter the height and width of the rectangle separated by a space: 30 40
 * The expected value for area is: 1200
 * Your calculated value of the area is; 1200
 * The expected value for perimeter is: 140
 * Your calculated value of the area is; 140
 * Enter the height and width of the rectangle separated by a space: -29 45
 * Input value invalid: Negative numbers unallowed
 * Enter the height and width of the rectangle separated by a space: -2 -6
 * Input value invalid: Negative numbers unallowed
 *
 * Process finished with exit code 0
 */